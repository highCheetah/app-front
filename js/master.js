'use strict';
const searchFilter = [];
                                                            /***********  DOM Elements **************/
// Filter Btn                                                            
const filterBtn = document.querySelector('button#filterBtn');
filterBtn.state = false;
filterBtn.addEventListener('click', resolveFilterDrop);
// Filter Btn Drop items
const filterDropItems = filterBtn.nextElementSibling.children;
for (let i = 0; i < filterDropItems.length; i++) {
    filterDropItems[i].state = false;
    filterDropItems[i].addEventListener('click', resolveFilterMiniDrop);
}    
// Date Button
const dateBtn = document.querySelector('button#dateBtn');
dateBtn.addEventListener('click', resolveDateDrop);
dateBtn.state = false;
// Filter Before Search Section
const filterResults = document.querySelector('.filter-results');


                                                            /***********  DOM Functions **************/
// Dropdown under filter buttons
function resolveFilterDrop() {

    if (!this.state) {
        this.children[0].className = "fa fa-angle-double-up";
        this.nextElementSibling.style.display = 'block';
        this.state = true;
    } else {
        filterBtn.children[0].className = "fa fa-angle-double-down";
        this.nextElementSibling.style.display = 'none';
        this.state = false;
    }
}
// Dropdown under filter button items
function resolveFilterMiniDrop() {

    if (event.target.tagName === 'INPUT') return;
    else {
        if (!this.state) {
            this.children[0].className = "fa fa-angle-double-up";
            // adjust display for radio btn drops
            if (this.id === 'radio-btns-drop') {
                this.children[1].style.display = 'block';
            } else {
                this.children[1].style.display = 'flex';
            }
            this.children[1].children[0].value = '';
            this.state = true;
        } else {
            // update filter
            if (event.target.tagName === 'BUTTON') {
                const newFilter = {};
                if (event.target.getAttribute('btn-purpose') === 'order-btn') {
                    newFilter.type = document.querySelector('input[name="orderStatus1"][value="choose"]').checked ? 'choose' : 'ignore';
                    newFilter.value = document.querySelector('input[name="orderStatus2"][value="finished"]').checked ? 'finished' : 'closed';
                } else {
                        newFilter.type = event.target.previousElementSibling.name;
                        newFilter.value = event.target.previousElementSibling.value;
                }
                updateFilter(newFilter);
            }
            this.children[0].className = "fa fa-angle-double-down";
            this.children[1].style.display = 'none';
            this.state = false;
        }  
    }
}
// Dropdown under date button
function resolveDateDrop() {

    if (!this.state) {
        this.children[0].className = "fa fa-angle-double-up";
        this.nextElementSibling.style.display = 'block';
        this.state = true;
    } else {
        this.children[0].className = "fa fa-angle-double-down";
        this.nextElementSibling.style.display = 'none';
        this.state = false;
    }
    const dateApplyBtn = document.querySelector('button[btn-purpose="date-btn"]');
    dateApplyBtn.addEventListener('click', resolveDateMiniDrop);
}
// handling date button apply
function resolveDateMiniDrop() {

    const newFilter = {};
    const radioBtns = document.querySelectorAll('input[name="dateType"]');
    let radioVal;
    for (let i = 0; i < radioBtns.length; i++) {
        if (radioBtns[i].checked) {
            console.log(radioBtns[i])
            radioVal = radioBtns[i].value;
        }
    }
    newFilter.type = radioVal;
    newFilter.value1 = document.querySelector('input[name="startDate"]').value;
    newFilter.value2 = document.querySelector('input[name="endDate"]').value;
    updateFilter(newFilter);
}
// Render filters user has selected
function renderUserFilters () {

    let str = '';
    for (let i = 0; i < searchFilter.length; i++) {
        str += '<div>' + searchFilter[i].type + ' : ' + searchFilter[i].value + '</div>'
    }
    filterResults.innerHTML = str;
    filterResults.style.display = 'block';
}
// Render page entries by filter
function renderEntries() {

}



                                                            /***********  Logic Functions **************/
function updateFilter(newFilter) {

    console.log(newFilter)
    searchFilter.push(newFilter);
    renderUserFilters();
}








/***********     ***********/
/***********     ***********/
/***********     ***********/
const views =  {
    views: {
        'orders': {
            element: document.querySelector('section[data-view-name="orders"]'),
            active: true
        },
        'general-invoice': {
            element: document.querySelector('[data-view-name="general-invoice"]'),
            active: false,
        },
        'single-invoice': {
            element: document.querySelector('[data-view-name="single-invoice"]'),
            active: false,
        },
        'bulk-invoice': {
            element: document.querySelector('[data-view-name="bulk-invoice"]'),
            active: false,
        }
    },
    render(name) {

        for (let view in this.views) {
            this.views[view].element.style.display = 'none';
        }
        this.views[name].element.style.display = 'block';
        drops.shutDown(null);
    }
};
/***********     ***********/
const drops = {
    drops: {
        'invoice' : {
            element: document.querySelector('ul[data-drop-name="invoice"]'),
            active: false
        },
    },
    render(dropName) {
        
        for (let drop in this.drops) {
            if (drop === dropName) { 
                this.drops[drop].element.active = !this.drops[drop].element.active;
                this.drops[drop].element.style.display = this.drops[drop].element.active ? 'block' : 'none';
                return;
            }
        }
    },
    shutDown() {

        for (let drop in this.drops) {
            this.drops[drop].element.active = false;
            this.drops[drop].element.style.display = 'none';
        }
    }
};
/***********     ***********/
const navBtns = {
    buttons: {
        'orders': {
            element: document.querySelector('div[data-view-target="orders"]'),
        },
        'invoice': {
            element: document.querySelector('div[data-drop-target="invoice"]'),
        }
    },
    click(elem) {
        
        const target = elem.getAttribute('data-view-target') 
            ? 
                {
                    key: 'data-view-target',
                    value: elem.getAttribute('data-view-target')  
                }
            :
                {
                    key: 'data-drop-target',
                    value: elem.getAttribute('data-drop-target')  
                };
        for (let btn in this.buttons) {
            this.buttons[btn].element.classList.remove('focus-gold');
        }
        this.buttons[target.value].element.classList.add('focus-gold');
        if (target.key.indexOf('view') !== -1) {
            views.render(target.value);
            return;
        } else {
            drops.render(target.value);
        }

    }
};
for (let btn in navBtns.buttons) {
    navBtns.buttons[btn].element.addEventListener('click', function() { navBtns.click(this);});
};
/***********     ***********/
const dropBtns = {
    buttons: {
        'general': {
            element: document.querySelector('li[data-view-target="general-invoice"]'),
        },
        'single': {
            element: document.querySelector('li[data-view-target="single-invoice"]'),
        },
        'bulk': {
            element: document.querySelector('li[data-view-target="bulk-invoice"]'),
        }
    },
    click(target) {

        views.render(target);
        // for (let btn in this.buttons) {
        //     this.buttons[btn].element.classList.remove('focus-gold');
        // }
        // this.buttons[target.value].element.classList.add('focus-gold');
        // if (target.key.indexOf('view') !== -1) {
        //     views.render(target.value);
        //     return;
        // } else {
        //     drops.render(target.value);
        // }
    }
};
for (let btn in dropBtns.buttons) {
    dropBtns.buttons[btn].element.addEventListener('click', function() { dropBtns.click(this.getAttribute('data-view-target'));});
};












// add event listeners when object is intialized
// also fire if specific dynamic style should be applied right away


// svaki kontroler kojem viewu pripada polje ( 4 viewa ovde )